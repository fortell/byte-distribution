# Dstribution of Time To last byte per Object size #

This application generates com.akamai.bytedistribution.chart which contains distribution of Time To Last Byte per Object size. 


### How do I get set up? ###

It contains mvn configuration. You can compile it by mvn install .

### Usage ###

First parameter when you run it form command line is path to com.akamai.bytedistribution.csv file.

Example of csv format

```
http_status,obj_sz,req_time,transfer_time,ts,turn_time
206,589926,0,768,1452106800.003,1
```

Chart is generated as test.png file in current folder.

To use it in a code first you need to call 

```java
List<TtlbPerObjectSize> groupedByTtlb = new com.akamai.bytedistribution.processor.TtlbPerSizeProcessor().process(csvFilePath);
```

as a result you get a list of agregated data by ttlb per object size.

To generate a char pass a result from com.akamai.bytedistribution.processor.TtlbPerSizeProcessor to method in DistributionBar : 

```java
new DistributionBar().drawChart(groupedByTtlb, imageFileName);
```

### Deploy of application ###

Application is deployed on heroku. 

### Production ###

Application is available here : https://byte-distribution.herokuapp.com/

### API documentation ###

API documentation is available onlie : https://byte-distribution.herokuapp.com/swagger-ui.html