package com.akamai.bytedistribution;

import com.akamai.bytedistribution.chart.DistributionBar;
import com.akamai.bytedistribution.collector.TtlbPerObjectSize;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.MultipartStream;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

@SpringBootApplication
@Configuration
public class Main {

    public static void main(String[] args) {
        createDirectories();

        SpringApplication.run(Main.class, args);

    }

    private static void createDirectories() {
        File upload = new File("upload");
        if (!upload.exists()) {
            upload.mkdir();
        }

        File publicDir = new File("public");
        if (!publicDir.exists()) {
            publicDir.mkdir();
            new File("public/images").mkdir();
        }
    }

}
