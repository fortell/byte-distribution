package com.akamai.bytedistribution.chart;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class DistributionBarRequest {

    String csvFileName;

}
