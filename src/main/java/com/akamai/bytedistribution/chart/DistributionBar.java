package com.akamai.bytedistribution.chart;

import com.akamai.bytedistribution.collector.TtlbPerObjectSize;
import de.erichseifert.gral.data.DataTable;
import de.erichseifert.gral.graphics.Insets2D;
import de.erichseifert.gral.io.plots.DrawableWriter;
import de.erichseifert.gral.io.plots.DrawableWriterFactory;
import de.erichseifert.gral.plots.BarPlot;
import de.erichseifert.gral.util.GraphicsUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.stream.IntStream;

@Component
@Slf4j
public class DistributionBar {

    /**
     * Generates chart based on TtlbPerObjectSize objects
     * @param tposList list of TtlbPerObjectSize objects
     * @param fileName path to output file with chart
     * @return path to generated chart
     * @throws IOException
     */
    public String drawChart(List<TtlbPerObjectSize> tposList, String fileName) throws IOException {

        String imageFileName = getImagePath(fileName);
        DataTable[] dataTables = new DataTable[5];
        Color[] colors = new Color[] {Color.GREEN, Color.BLUE, Color.RED, Color.YELLOW, Color.PINK};
        IntStream.range(0, 5).forEach(i ->  {
            dataTables[i] = new DataTable(Long.class, Float.class);
            tposList.stream().filter(t -> t.getItemsInSample() > 0 && t.getSizeClass() == i + 1).forEach( tpos-> dataTables[i].add(tpos.getTtlb(), tpos.getPercentageOfSample()));
        });
        BarPlot plot = new BarPlot(dataTables[0], dataTables[1], dataTables[2], dataTables[3], dataTables[4]);

        // Format plot
        plot.setInsets(new Insets2D.Double(140.0, 40.0, 40.0, 40.0));
        plot.setBarWidth(0.075);
        plot.autoscaleAxis("x");

        // Format bars
        IntStream.range(0, 5).forEach(i -> {
            BarPlot.BarRenderer pointRenderer = (BarPlot.BarRenderer) plot.getPointRenderers(dataTables[i]).get(0);
            setPlotColor(pointRenderer, colors[i]);
        });

        try {
            File file = new File(imageFileName);
            DrawableWriter writer = DrawableWriterFactory.getInstance().get("image/png");
            writer.write(plot, new FileOutputStream(file), 800, 600);
        } catch (IOException e) {
            log.error("Error during creating chart", e);
            throw e;
        }

        return imageFileName;
    }

    public static String getImagePath(String csvFileName) {
        return "public/images/" + csvFileName + ".png";
    }

    private void setPlotColor(BarPlot.BarRenderer pointRenderer, Color color) {
        pointRenderer.setColor(
                new LinearGradientPaint(0f,0f, 0f,1f,
                        new float[] { 0.0f, 1.0f },
                        new Color[] { color, GraphicsUtils.deriveBrighter(color) }
                )
        );
        pointRenderer.setBorderStroke(new BasicStroke(3f));
        pointRenderer.setBorderColor(
                new LinearGradientPaint(0f,0f, 0f,1f,
                        new float[] { 0.0f, 1.0f },
                        new Color[] { GraphicsUtils.deriveBrighter(color), color }
                )
        );
        pointRenderer.setValueVisible(false);
        pointRenderer.setValueColumn(2);
    }
}
