package com.akamai.bytedistribution.chart;

import com.akamai.bytedistribution.processor.TtlbPerSizeProcessor;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URI;

@RestController
@Api(description = "Generates bar chart with ttlb per object size")
@Slf4j
public class DistributionBarController {

    final String publicPath = "public/";

    @Autowired
    DistributionBar distributionBar;

    @Autowired
    TtlbPerSizeProcessor ttlbPerSizeProcessor;

    @PostMapping("/chart")
    @ApiOperation(value = "Generates chart with ttlb per object size class",
            notes = "Generates png image")
    @ApiResponses(value = {
            @ApiResponse(code = 201, responseHeaders = {
                    @ResponseHeader(name = "location", description = "URI to created chart.", response = String.class),
            }, message = "Created")})
    public ResponseEntity<Void> generateChart(@RequestBody DistributionBarRequest distributionBarRequest) {
        try {
            String csvFileName = distributionBarRequest.csvFileName;
            String imageFileName = distributionBar.drawChart( ttlbPerSizeProcessor.process(csvFileName), csvFileName);

            return ResponseEntity.created( URI.create(preparePublicPath( imageFileName )) ).build();
        } catch (IOException e) {
            log.error("Error during creating chart", e);
            return ResponseEntity.unprocessableEntity().build();
        }
    }

    private String preparePublicPath( String path ) {
        if (path.startsWith(publicPath)) {
            return path.substring(publicPath.length());
        }
        return path;
    }
}
