package com.akamai.bytedistribution.collector;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@AllArgsConstructor
@EqualsAndHashCode
public class TtlbPerObjectSize {
    private long ttlb;
    private long itemsInSample;
    private float percentageOfSample;
    private long sizeClass;
}
