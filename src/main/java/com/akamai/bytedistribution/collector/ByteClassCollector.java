package com.akamai.bytedistribution.collector;

import com.akamai.bytedistribution.csv.ByteDistributionDataRow;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;


/**
 * Class for collecting values by ttlb
 */
@Component
public class ByteClassCollector {

    /**
     * Collects data by ttlb
     * @param data rows with data
     * @param collector used for calculating value per ttlb
     * @return List of tpos objects
     */
    public List<TtlbPerObjectSize> collect(List<ByteDistributionDataRow> data, Collector<Object, ?, Long> collector) {

        Map<String, Long> counted = data.stream()
                .collect(Collectors.groupingBy((r) -> r.getKey(), collector));
        Long elements = data.stream().count();
        return data.stream().map((e) -> new TtlbPerObjectSize(e.getTtlb(),
                counted.get(e.getKey()), Long.valueOf(counted.get(e.getKey())).floatValue() / elements * 100 , e.getSizeClass() ))
                .distinct().collect(Collectors.toList());
    }
}
