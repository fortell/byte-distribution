package com.akamai.bytedistribution.collector;

import com.akamai.bytedistribution.csv.ByteDistributionDataRow;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.stream.Collectors;

class ByteClassCollectorTest {

    @Test
    public void collectDataFromCsvData() {
        ByteClassCollector byteClassCollector = new ByteClassCollector();

        ArrayList<ByteDistributionDataRow> testData = new ArrayList<>();
        ByteDistributionDataRow bddr1 = new ByteDistributionDataRow();
        bddr1.setObjectSize(990);
        bddr1.setReqTime(10);
        bddr1.setTransferTime(100);
        bddr1.setTurnTime(100);
        testData.add(bddr1);

        ByteDistributionDataRow bddr2 = new ByteDistributionDataRow();
        bddr2.setObjectSize(880);
        bddr2.setReqTime(10);
        bddr2.setTransferTime(100);
        bddr2.setTurnTime(100);
        testData.add(bddr2);

        ByteDistributionDataRow bddr3 = new ByteDistributionDataRow();
        bddr3.setObjectSize(500880);
        bddr3.setReqTime(10);
        bddr3.setTransferTime(100);
        bddr3.setTurnTime(100);
        testData.add(bddr3);

        java.util.List<TtlbPerObjectSize> ttlbPerObjectSizeList = byteClassCollector.collect(testData, Collectors.counting());

        Assertions.assertEquals( 2, ttlbPerObjectSizeList.size() );
        Assertions.assertEquals( 210, ttlbPerObjectSizeList.get(0).getTtlb() );
        Assertions.assertEquals( 66.66, ttlbPerObjectSizeList.get(0).getPercentageOfSample(), 0.01 );
        Assertions.assertEquals( 33.33, ttlbPerObjectSizeList.get(1).getPercentageOfSample(), 0.01 );
    }
}