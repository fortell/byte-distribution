package com.akamai.bytedistribution.processor;

import com.akamai.bytedistribution.collector.TtlbPerObjectSize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@Slf4j
@Api(description = "Process all data from CSV file and returns agregated data by ttlb and object class size.")
public class ProcessorController{

    @Autowired
    TtlbPerSizeProcessor ttlbPerSizeProcessor;

    @GetMapping("/")
    @ApiOperation(value = "Gets list of ttlb objects, generated from CSV file which should be uploaded previously",
    response = TtlbPerObjectSize.class, responseContainer = "List")
    public ResponseEntity<List<TtlbPerObjectSize>> list(
            @ApiParam(value = "CSV filename that will be processed", required = true)
            @RequestParam String csvFileName) {
        try {
            List<TtlbPerObjectSize> groupedByTtlb = ttlbPerSizeProcessor.process(csvFileName);
            return ResponseEntity.ok(groupedByTtlb);
        } catch (IOException e) {
            log.error("Exception during parsing CSV:", e);
            return ResponseEntity.notFound().build();
        }
    }

}
