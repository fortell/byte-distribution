package com.akamai.bytedistribution.processor;

import com.akamai.bytedistribution.collector.ByteClassCollector;
import com.akamai.bytedistribution.collector.TtlbPerObjectSize;
import com.akamai.bytedistribution.csv.ByteDistributionDataRow;
import com.akamai.bytedistribution.csv.CsvParser;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Component
public class TtlbPerSizeProcessor {

    @Autowired
    private CsvParser csvParser;

    @Autowired
    private ByteClassCollector byteClassCollector;

    /**
     * Reads CSV file and generates aggregated data per ttlb with object size class
     * @param inputFileName path to CSV file
     * @return list of TtlbPerObjectSize objects containds calculated data
     * @throws IOException
     */
    public List<TtlbPerObjectSize> process(String inputFileName) throws IOException {
        List<ByteDistributionDataRow> byteDistributionDataRows = csvParser.processInputFile(inputFileName);
        byteDistributionDataRows = byteDistributionDataRows.stream().collect(Collectors.toList());
        final List<TtlbPerObjectSize> groupedByTtlb = byteClassCollector.collect(byteDistributionDataRows, Collectors.counting());

        groupedByTtlb.sort(Comparator.comparing(TtlbPerObjectSize::getTtlb).thenComparing(TtlbPerObjectSize::getSizeClass));
        return groupedByTtlb;
    }
}
