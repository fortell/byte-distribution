package com.akamai.bytedistribution.csv;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@RestController
@Api(basePath = "/", description = "Upload CSV files with validation")
@Slf4j
public class UploadController {

    @Autowired
    private CsvParser csvParser;

    @Autowired
    private CsvDataSource csvDataSource;

    @PostMapping("/")
    public ResponseEntity<Void> handleFileUpload(@RequestBody CsvUpload csvData ) {

        try {
            File csvFile = new File( CsvDataSource.getInternalFileName(csvData.getFileName()));
            if (csvFile.exists()) {
                log.error("File with uploaded name already exists.");
                return ResponseEntity.status(HttpStatus.CONFLICT).build();
            }

            try {
                csvParser.getByteDistributionDataRows(
                        new ByteArrayInputStream(csvData.getFileContent().getBytes(StandardCharsets.UTF_8.name()))
                );
            } catch (NumberFormatException nfe) {
                log.error("There was error during parsing CSV file.", nfe);
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
            }
            csvDataSource.storeCsvFile(csvData);

        } catch (IOException e) {
            log.error("IO Exception during upload", e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        return ResponseEntity.ok().build();
    }


}