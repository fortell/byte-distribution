package com.akamai.bytedistribution.csv;

import lombok.Data;
import lombok.ToString;

/**
 * @author lukaszbancerowski
 * @since 2017-10-09
 */

@Data
@ToString
public class ByteDistributionDataRow {
    public String httpStatus;
    public long objectSize;
    public long reqTime;
    public long transferTime;
    public float ts;
    public long turnTime;

    public final long _100K = 100000;
    public final long _1M = 1000000;
    public final long _10M = 10000000;
    public final long _100M = 100000000;
    public final long _1G = 1000000000;


    public long getTtlb() {
     return reqTime + transferTime + turnTime;
    }

    public String getKey() {
        return getTtlb() + "_" + getSizeClass();
    }

    public int getSizeClass() {
        if (objectSize < _100K) {
            return 0;
        }
        if (objectSize >= _100K && objectSize < _1M) {
            return 1;
        }
        if (objectSize >= _1M && objectSize < _10M) {
            return 2;
        }
        if (objectSize >= _10M && objectSize < _100M) {
            return 3;
        }
        if (objectSize >= _100M && objectSize < _1G) {
            return 4;
        }
        return -1;
    }

}
