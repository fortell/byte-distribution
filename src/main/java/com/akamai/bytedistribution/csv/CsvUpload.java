package com.akamai.bytedistribution.csv;

import lombok.Data;

@Data
public class CsvUpload {

    String fileContent;
    String fileName;
}
