package com.akamai.bytedistribution.csv;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

class CsvParserTest {

    @Test
    public void generateByteDistributionDataRow() throws IOException {
        List<ByteDistributionDataRow> byteDistributionDataRows = new CsvParser().getByteDistributionDataRows(this.getClass().getResourceAsStream("/testlog.csv"));
        Assertions.assertEquals(1, byteDistributionDataRows.size());
        Assertions.assertEquals(769, byteDistributionDataRows.get(0).getTtlb());
    }
}