package com.akamai.bytedistribution.csv;

import org.springframework.stereotype.Component;

import java.io.*;

@Component
public class CsvDataSource {

    public void storeCsvFile(CsvUpload csvData) throws IOException {
        new FileWriter(getInternalFileName(csvData.getFileName())).append(csvData.getFileContent()).close();
    }

    public InputStream getInputStreamFromFile(String fileName) throws FileNotFoundException {
        File inputF = new File(getInternalFileName(fileName));
        InputStream inputStream = new FileInputStream(inputF);
        return inputStream;
    }

    public static String getInternalFileName(String fileName) {
        return "upload/" + fileName;
    }
}
