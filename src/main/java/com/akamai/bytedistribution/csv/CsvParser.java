package com.akamai.bytedistribution.csv;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.validation.constraints.Null;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class CsvParser {

    private static final java.lang.String COMMA = ",";

    @Autowired
    private CsvDataSource csvDataSource;

    /**
     * Parses CSV file and returns object with parsed data
     * @param inputFilePath path to CSV file
     * @return list of ByteDistributionDataRow objects with data from CSV file
     * @throws IOException
     */
    public List<ByteDistributionDataRow> processInputFile(String inputFilePath) throws IOException {
        List<ByteDistributionDataRow> inputList;
        try {
            inputList = getByteDistributionDataRows(csvDataSource.getInputStreamFromFile(inputFilePath));
        } catch (IOException e) {
            throw e;
        }
        return inputList;
    }

    public List<ByteDistributionDataRow> getByteDistributionDataRows(InputStream inputFS) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(inputFS));
        final Stream<ByteDistributionDataRow> byteDistributionDataRowStream = br.lines().skip(1).flatMap(mapToItem);
        List<ByteDistributionDataRow> inputList = byteDistributionDataRowStream.collect(Collectors.toList());
        br.close();
        return inputList;
    }

    private Function<String, Stream<ByteDistributionDataRow>> mapToItem = (String line) -> {
        String[] values = line.split(COMMA);
        try {
            return Stream.of(parseLine(line, values));
        } catch (CsvParserException e) {
            return Stream.empty();
        }
    };

    private ByteDistributionDataRow parseLine(String line, String[] csvValues) throws CsvParserException {
        ByteDistributionDataRow item = new ByteDistributionDataRow();
        try {
            item.httpStatus = csvValues[0];
            item.objectSize = Long.parseLong(csvValues[1]);
            item.reqTime = Long.parseLong(csvValues[2]);
            item.transferTime = Long.parseLong(csvValues[3]);
            item.ts = Float.parseFloat(csvValues[4]);
            item.turnTime = Long.parseLong(csvValues[5]);
        } catch (NumberFormatException|NullPointerException ex) {
            throw new CsvParserException(ex, line);
        }
        return item;
    }
}

