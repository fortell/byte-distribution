package com.akamai.bytedistribution.csv;

import lombok.Data;
import lombok.Setter;

@Setter
public class CsvParserException extends Exception {

    public CsvParserException(Exception exception, String csvLine) {
        super(String.format("Incorrect line in CSV file %s", csvLine), exception);
    }

}
